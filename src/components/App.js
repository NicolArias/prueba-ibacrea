import React, {useEffect, useState} from 'react';
import '../styles/styles.css';

export const App = () => {
    const [trabajadores, setTrabajadores] = useState([]);
    const [names, setNames] = useState("");
    const [infTrabajador, setInfTrabajador] = useState([]);
    const [news, setNew] = useState([]);
    const [news2, setNew2] = useState([]);
    const [empleadosnuevos, setEmpleadosNuevos] = useState('Si');
    const [listanuevos, setListaNuevos] = useState([]);
    const [mostrarEmpleadosS, setMostrarEmpleadosS] = useState([]);
    
    useEffect(() => {
        fetchData()
    }, [])
    
    const fetchData = async () => {
        const data = await fetch('https://raw.githubusercontent.com/sapardo10/content/master/RH.json')
        const trabajador = await data.json()
        setTrabajadores(trabajador.employees)

        // trabajadores.sort((a, b) => a.wage - b.wage);
        // setTrabajadores(trabajadores)
        // console.log("Después de ordenar: ", trabajadores);
        // console.log(trabajadores)
    }

    const agregarNuevo = (name) => {
        name == "" ? console.log('vacio') :
        trabajadores.filter((nid) => {
            if(name != "") {
               if (nid.name === name){
                return nid
            }}
        }).map(trabajadores => (
            trabajadores.nuevo = "Si"
        ))
        listNew()
        // console.log(trabajadores)
    } 

    const quitarNuevo = (name) => {
        name == "" ? console.log('vacio') :
        trabajadores.filter((nid) => {
            if(name != "") {
               if (nid.name === name){
                return nid
            }}
        }).map(trabajadores => (
            trabajadores.nuevo = "No"
        ))
        listNew()
        // console.log(trabajadores)
    } 


    const listNew = () => {
        const xd = trabajadores.filter(e => e.nuevo === empleadosnuevos)
        setListaNuevos(xd)
    }

    const MostrarEmpleados = (item) => {
        // console.log(item)
        const mostrarE = trabajadores.filter(e => e.name === item)
        console.log(mostrarE)
        setMostrarEmpleadosS(mostrarE)

        mostrarEmpleadosS.filter((emp) => {
            if (emp.name === item){
                return emp.employees
            }
            console.log('emp',emp)
            setInfTrabajador(emp.employees)
        })
        console.log(infTrabajador)
    }

    const order = (asc) => {
        asc === 'asc' ?
        trabajadores.sort((a, b) => a.wage - b.wage)
        :
        trabajadores.sort((a, b) => b.wage - a.wage);
        // setNew2(trabajadores)

        setNew(trabajadores)
        console.log("Después de ordenar: ", news);
    }

return (
    <>
    <div className="container">
        <h1>
            PRUEBA - REACT JS
        </h1>
        <br/>
        <div className="inp">
            <input 
                type="text" 
                className="form-control" 
                placeholder="Buscar" 
                onChange={(e) => setNames (e.target.value)}
            />
            <i className="fa fa-long-arrow-down" aria-hidden="true" onClick={() => order('asc')}></i>&nbsp;
            <i className="fa fa-long-arrow-up" aria-hidden="true" onClick={() => order('desc')}></i> 
            {/* <button className="button_nuevo" onClick={() => order('desc')}>order</button> */}
        </div>
        
        <br/><br/>
        <ul className="list-group list-group-flush">
            { 
                trabajadores.filter((nam) => {
                    if(names == "") {
                        return nam
                    } else if (nam.name.toLowerCase().includes(names.toLowerCase())){
                        return nam
                    }
                }).map(item => (
                    <li className="list-group-item" key={item.id}>
                        <button 
                            data-toggle="dropdown" 
                            className="dropdown" 
                            onClick={() => MostrarEmpleados(item.name)}
                        >{item.name}
                        </button>
                        {/* <i className="fa fa-long-arrow-right" aria-hidden="true"></i> */}
                        <label className="wage">---${item.wage}</label>
                        <div className="dropdown-menu">
                            <ul>
                                {
                                    trabajadores.filter((nam2) => {
                                        if(item.name != "") {
                                            if (nam2.name.toLowerCase().includes(item.name.toLowerCase())){
                                                return nam2
                                            }
                                        }
                                    }).map(item => (
                                        <li className="dropdown-item" key={item.id}>
                                            Id: {item.id}<br/>
                                            Nombre: {item.name}<br/>
                                            Posición: {item.position}<br/>
                                            Sueldo: {item.wage}<br/>
                                            <button 
                                                className="button_nuevo"
                                                onClick={() => agregarNuevo(item.name)} 
                                            >Añadir Nuevo
                                            </button><br/>
                                            <button 
                                                className="button_nuevo"
                                                onClick={() => quitarNuevo(item.name)} 
                                            >Quitar Nuevo
                                            </button><br/>
                                            Empleados: 
                                            <ul>
                                                {
                                                    item.employees.length === 0 ? 'No tiene' :
                                                    infTrabajador.map(item => (
                                                        <li key={item.id}>
                                                            {item.id}-{item.name}
                                                        </li>
                                                    ))
                                                }
                                            </ul>
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    </li>
                ))
            }
        </ul>
        <hr/>
        <br/>
        <h3>
            Lista de empleados nuevos
        </h3>
        <br/>
        <ul>
            { 
                listanuevos.map(item => (
                    <li key={item.id}>
                        <a>{item.name}</a>
                    </li>
                ))
            }
        </ul>
        <br/>
        <br/>
        
    </div>        
    <hr/>
        <div className="csd">
            <p>© Prueba</p>
        </div>
    </>
)
}

